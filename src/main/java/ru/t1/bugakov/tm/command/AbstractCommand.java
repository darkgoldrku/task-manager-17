package ru.t1.bugakov.tm.command;

import ru.t1.bugakov.tm.api.command.ICommand;
import ru.t1.bugakov.tm.api.service.IServiceLocator;

public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    public void setServiceLocator(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    @Override
    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        String result = "";
        if(name != null && !name.isEmpty()) result += name + " : ";
        if(argument != null && !argument.isEmpty()) result += argument + " : ";
        if(description != null && !description.isEmpty()) result += description;
        return result;
    }
}
