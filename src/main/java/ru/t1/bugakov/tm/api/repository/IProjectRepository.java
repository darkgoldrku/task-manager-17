package ru.t1.bugakov.tm.api.repository;

import ru.t1.bugakov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    boolean existsById(String id);

    List<Project> findAll();

    List<Project> findAll(Comparator comparator);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    void clear();

    void remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

}
