package ru.t1.bugakov.tm.api.service;

import ru.t1.bugakov.tm.enumerated.Sort;
import ru.t1.bugakov.tm.enumerated.Status;
import ru.t1.bugakov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    Task create(String name, String description);

    void add(Task task);

    List<Task> findAll();

    List<Task> findAll(Sort sort);

    List<Task> findAll(Comparator comparator);

    List<Task> findAllByProjectId(String projectId);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    void clear();

    Task removeById(String id);

    Task removeByIndex(Integer index);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

}
