package ru.t1.bugakov.tm.api.service;

public interface IServiceLocator {

    ICommandService getCommandService();

    ILoggerService getLoggerService();

    IProjectService getProjectService();

    ITaskService getTaskService();

    IProjectTaskService getProjectTaskService();

}
